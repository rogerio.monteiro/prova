if [ $# -ne 4 ]; then 
    echo "digite 4 parametros: "
    exit 1
fi 

for dir_name in "$@"; do 
    mkdir "$dir_name"
    echo "criando $dir_name"
    echo "$dir_name" > "$dir_name/README.md"
    echo "criando o readme $dir_name"
done 

echo "terminou"
