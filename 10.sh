echo "Nome do arquivo: "
read nome_arquivo

if [ -f "$nome_arquivo" ]; then
    linhas_vitoria=$(grep -c "vitoria" "$nome_arquivo")
    echo "o arquivo $nome_arquivo possui $linhas_vitoria linha com a palavra"
else
    echo "arquivo não encontrado"
fi
