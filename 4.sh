echo "processador: "
cat /proc/cpuinfo | grep "model name \|cpu cores\|cpu MHz\|cache size"

echo ""

echo "memoria ram"
free -h 

echo ""

echo "disco rígido"
df -h 

echo ""

echo "placa de video"
lspci | grep VGA

echo ""

echo "USB: "
lsusb

echo ""

echo "placa mãe"
dmidecode -t baseboard

echo ""

echo "BIOS:"
dmidecode -t bios
