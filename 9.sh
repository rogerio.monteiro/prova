if [ $# -ne 4 ]; then 
    echo "passe 3 parametros: " > &2
    exit 1
fi

total_linhas = 0

for arquivo in "$@"; do 
    if [ ! -f "$arquivo" ]; then 
        echo "o arquivo '$arquivo' não existe" > $2
        exit 1 
    fi 
    num_linhas=$(wc -e < "$arquivo")
    total_linhas=$((total_linhas + num_linhas))

done 

echo "$total_linhas"./5
